#!/bin/sh
APP_PATH="/usr/app"
INSTALL_PATH="/opt/ecolink-update/ecolink"

echo "installation start..."

if [ -e $INSTALL_PATH ]; then
	cd $INSTALL_PATH
else
	echo "warning: remote update dir not exsited"
fi

if [ -e .ecolink.ver ]; then 
	echo "installing .ecolink.ver"
	cat .ecolink.ver
	cp .ecolink.ver $APP_PATH/
else
	echo "lack of .ecolink.ver!"
	cd -
	exit
fi
if [ -e ecolink.bin ]; then 
	echo "installing ecolink.bin..."
	cp ecolink.bin $APP_PATH/
	#cp ecolink.bin $APP_PATH/phonelink/mirrorLinkApp
fi
if [ -e ledaemon ]; then
	echo "installing ledaemon..."
	cp ledaemon $APP_PATH/
fi

if [ -e libEcolink.so ]; then

	echo "installing libEcolink.so..."
	cp libEcolink.so $APP_PATH/
fi

if [ -e libjpeglib70_turbo.so ]; then
	echo "installing libjpeglib70_turbo.so..."
	cp libjpeglib70_turbo.so $APP_PATH/
fi
if [ -e adb ]; then

	echo "installing adb..."
	cp adb /usr/bin
fi

if [ -e CarIphone ]; then
	echo "installing CarIphone..."
	cp CarIphone $APP_PATH/
fi

if [ -e unix_socket.txt ]; then
	echo "installing unix_socket.txt..."
	cp unix_socket.txt $APP_PATH/
fi

if [ -e server.bin ]; then
	echo "installing server.bin..."
	cp server.bin $APP_PATH/
fi


if [ -e le ]; then
	echo "installing le/res/..."
	cp le $APP_PATH/res/ -rf
fi


if [ -e adb_usb.ini ]; then
	echo "installing adb_usb_ini..."
	mkdir -p ~/.android
	cp adb_usb.ini ~/.android/
	cp $APP_PATH/res/le/files/adb_usb.ini ~/.android -f
fi

if [ -e libjpeg-turbo-1.4.2.tar.gz ]; then
	echo "installing libjpeg-turbo..."
	tar -zxvf libjpeg-turbo-1.4.2.tar.gz -C /usr/local/
fi

mkdir -p /opt/ecolink-update/

chmod +x $APP_PATH/ecolink.bin
chmod +x $APP_PATH/ledaemon
chmod +x $APP_PATH/server.bin
chmod +x $APP_PATH/CarIphone
chmod +x /usr/bin/adb

cd -

