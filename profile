PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/minigui/bin

PS1='[\u@\h \W]\$ '

export PATH

alias ll='ls -l'
alias la='ll -a'

export PS1='\u@\h \w$ '
export PS2='> '
export PS3='? '
export PS4='[$LINENO]+'

export GST_PLUGIN_PATH=/usr/lib/fsl_mm_linux/lib/gstreamer-0.10
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/libjpeg-turbo-1.4.2/freescale/lib:/usr/local/minigui/lib:/usr/app/lib

export TSLIB_ROOT=/usr/local/minigui
export TSLIB_TSDEVICE=/dev/input/event1
export TSLIB_CALIBFILE=/etc/pointercal
export TSLIB_CONFFILE=$TSLIB_ROOT/etc/ts.conf 
export TSLIB_PLUGINDIR=$TSLIB_ROOT/lib/ts
export TSLIB_CONSOLEDEVICE=none
export TSLIB_FBDEVICE=/dev/fb0 



export IPOD_LOG_LEVEL=’2’
export IPOD_ACCESSORY_CHIP_I2C_STR="/dev/i2c-0"
export IPOD_PLAY_CARD_NAME=nullaudio

export GST_PROB_WAY=""
export MEDIA_PIC_PATH="/dev/shm"
export BT_UART_DEBUG="on"
export BT_CONTACTS_FILE_SAVE="on"
export BT_NAME="G29"
export BT_PROJECT_NAME="G29"
export BT_DATA_SAVE="shm"
#export LD_PRELOAD=/usr/app/lib/preloadable_libiconv.so



//usleep 500000
if [ -e /usr/local/minigui/var/tmp/mginit ]; then
	rm -f /usr/local/minigui/var/tmp/mginit
fi

if [ -e /usr/local/minigui/var/tmp/minigui ]; then
	rm -f /usr/local/minigui/var/tmp/minigui
fi
let val=1
while [ -e  /sys/devices/platform/fsl-ehci.1/usb2/2-1 ]
do
usleep 50000
let "val+=1"
if [ -e /dev/sda1 ]; then
	echo "=============/dev/sda1================="
	mount -o iocharset=utf8 /dev/sda1 /mnt/usb
	break
fi

if [ -e /dev/sda4 ]; then
        echo "=========/dev/sda4==========="
	mount -o iocharset=utf8 /dev/sda4 /mnt/usb
	break
fi

if [ -e /dev/sda ]; then
	echo "============/dev/sda==========="
	mount -o iocharset=utf8 /dev/sda /mnt/usb
	break
fi
if [ "$val" -eq 10 ];then
        break
fi
done

cd /usr/local/minigui/mginit
./mginit &
#usleep 500000

if [ -e /mnt/usb/update.bin ]; then
	cd /mnt/usb
	sleep 2;
	./update.bin 
	exit 0
fi

cd /usr/app/
source ./phonelink/setenv.sh
./ledaemon &
export ECOLINK_TRACE_CFG=all
./run_app.sh &
./gstServer.bin &
./run_btServer.sh &
