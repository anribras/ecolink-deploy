-----------------------------------------
LE linux ecolink released note
-----------------------------------------
- install instruction
  1. Decompress v1.0.tar.gz to any path. 
  2. Change into dir.
  3. Run ./install.sh
  4. Modify /etc/profile
    2.1 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/libjpeg-turbo-1.4.2/freescale/lib:/usr/local/minigui/lib:/usr/app/lib
	2.2 export ECOLINK_TRACE_CFG=all to get all the debug information,
		also can be ecolink,ledaemon,libso or any of them.
    2.3 make ledaemon run background in usr/app , add ./ledaemon &
  5. Reboot system.
------------------------------------------
released v1.0 
2016.5.4 by yangding@le.com
------------------------------------------
- support at least Android 4.4 version or more.
- not support iphone yet.
- not integrate with hangsheng app.bin yet.
------------------------------------------
released v1.1 
2016.5.13 by yangding@le.com
------------------------------------------
- add more andorid phone support
- add hangsheng ecolink protocl binded with  /var/tmp/mirrorlink.txt
- add auto hidden and show, set hidden at first 
- add help tips if phone not open usb debug mode
- optimize ledaemon and adb logic
- add iphone ,but not 100% ok yet.
------------------------------------------
released v1.2 
2016.5.27 by yangding@le.com
------------------------------------------
- iphone is all right
------------------------------------------
released v1.2.2 
2016.6.1 by yangding@le.com
------------------------------------------
- screnncap and micar not the best version.
- added hangsheng newest implementation and fix one bug;
- optimize connection time during pushing files
- optimize install.sh 
------------------------------------------
released v1.3 
2016.6.16 by yangding@le.com
------------------------------------------
- screnncap and micar currently the best.
- better compatability
- connection more stable
- not automatically show and hide, ecolink is controled by button 
------------------------------------------
released v1.3.2
2016.7.06 by yangding@le.com
------------------------------------------
- added Android/IOS remote updating functionality
- better debug logging
------------------------------------------
released v1.3.5
2016.7.14 by yangding@le.com
------------------------------------------
- fixed several bugs
- iphone stops mirroring when hidden
------------------------------------------
released v1.3.6
2016.8.10 by yangding@le.com
------------------------------------------
- fixed several Android phone can not send touch events
- fixed bug when press link or return button on car too frequently
- updated apk to the newest v2.1.20
------------------------------------------
released v1.3.7
2016.8.22 by yangding@le.com
------------------------------------------
- fixed bug of screen splashed in some case;
------------------------------------------
released v1.3.8
2016.8.25 by yangding@le.com
------------------------------------------
- fixed a bug of home button;
- updated android apk to v2.2.2
------------------------------------------



